import operator
import numpy as np
import sys
import random
import os
import multiprocessing.managers
from multiprocessing import Process
import signal

from src.individual import Individual
from src.mutations.mutation_manager import MutationManager
from src.nst import NeuralStyleTransfer
from src.save import SaveManager

RANK_MAX = 9999


def sync_manager_init():
    """ catch Ctrl + C """
    signal.signal(signal.SIGINT, signal.SIG_IGN)


def verbose_print(verbose, data):
    if verbose:
        print(data)


class NeuralEvolutionaryArtStyleTransfer:
    def __init__(self, save_dir="save", save_file="main_save", load_file="main_save", continue_moea=False,
                 verbose=False):
        """
        :param save_dir: Directory holding the saves
        :param save_file: File where to save data in case the program is exited before the end of the algorithm
        :param load_file: File where the saves are loaded if continue_moea = True
        :param continue_moea: In case a non-terminated algorithm as been saved in save_file, continue to run it if True
        :param verbose: level of verbosity
        """
        self.save_dir = save_dir
        self.save_file = save_file
        self.load_file = load_file
        self.continue_moea = continue_moea
        self.initial_generation = 0
        self.initial_idx_population = 0
        self.initial_individual_A_done = False
        self.initial_individual_B_done = False
        self.mutation_details = {}
        self.first_individuals = []
        self.current_generation = 0
        self.current_cross_over = 0
        self.individuals = []
        self.selected_parents = None
        self.mutation_manager = None
        self.selection_x = 0
        self.unique = False

        # multiprocessing parameters
        manager = multiprocessing.managers.SyncManager()
        manager.start(sync_manager_init)
        self.new_generation = []
        self.done_generation = []
        self.processes = []

        # when continue_moea, check if the file to load exists
        full_path_save = os.path.join(self.save_dir, self.load_file)
        if self.continue_moea and not SaveManager.can_restore_save(full_path_save):
            print(f"Cannot restore save, file '{self.load_file}' not found", file=sys.stderr)
            exit(1)

        # load the file and the associated objects when continue
        if self.continue_moea:
            verbose_print(verbose, "Load saved data...")
            self.first_individuals, new_generation, self.mutation_details, steps, self.selected_parents = \
                SaveManager.load(self.save_dir, self.load_file, verbose=verbose)
            self.initial_generation = steps[0]
            self.done_generation = steps[1:]
            for n in new_generation:
                self.new_generation.append(n)

    @staticmethod
    def _selection(individuals, apply_phenotype_if_empty=True):
        """
            rank the population according to their objectives, and select hald of them
            in case of equality, sparsity is used
        """
        selected = []
        # we select half of the population
        to_select = int(len(individuals) / 2)

        # we reset the rank of the individuals and compute phenotypes if needed
        for individual in individuals:
            individual.rank = RANK_MAX
            if apply_phenotype_if_empty and len(individual.objective_scores) == 0 or all(
                    [objective == 0 for objective in individual.objective_scores]):
                individual.phenotype()
        for rank in range(1, len(individuals) + 1):
            if len(selected) >= to_select:
                break

            # we select the current rank
            selected_in_rank = []
            remaining = [i for i in individuals if i not in selected]
            for idx, individual in enumerate(remaining):
                others = remaining[:idx] + remaining[idx + 1:]
                if not individual.dominated(others):
                    individual.rank = rank
                    selected_in_rank.append(individual)
            if len(selected) + len(selected_in_rank) <= to_select:
                selected += selected_in_rank

            # or we use sparsity
            else:
                for idx, s in enumerate(selected_in_rank):
                    s.others = selected_in_rank[:idx] + selected_in_rank[idx + 1:]
                selected_in_rank.sort(key=operator.methodcaller('sparsity'), reverse=True)
                selected += selected_in_rank[:(to_select - len(selected))]
        return selected

    @staticmethod
    def _apply_rank(individuals):
        """ return the selected individuals from selection and add the rank to all individuals """
        selected = NeuralEvolutionaryArtStyleTransfer._selection(individuals)
        rank_max = max(list(map(lambda y: y.rank, list(filter(lambda x: x.rank != RANK_MAX, individuals)))))
        for individual in individuals:
            if individual.rank == RANK_MAX:
                individual.rank = rank_max + 1
        return selected

    @staticmethod
    def _get_parents(individuals, return_couples=True, return_genotypes=True):
        """
            get a list containing all the individuals parents
            :param return_couples: return list of couples [p1, p2] (can be None)
            otherwise return a 1D list with all the parents (no None)
            :param return_genotypes: return only genotypes (otherwise whole parents)
        """
        _get = lambda i: i if not return_genotypes else i.genotype
        if return_couples:
            return [
                [None, None] if ind.parent1 is None or ind.parent2 is None
                else [_get(ind.parent1), _get(ind.parent2)] for ind in individuals
            ]
        else:
            parents_genotypes = [
                [] if ind.parent1 is None or ind.parent2 is None
                else [_get(ind.parent1), _get(ind.parent2)] for ind in individuals
            ]
            return [] if all([not x for x in parents_genotypes]) else list(np.concatenate(parents_genotypes).flat)

    @staticmethod
    def _rank_selection(individuals, parents, check_both_way=False, min_value=10, difference=1):
        """ select 2 individuals using rank selection """
        random.shuffle(individuals)
        parents_genotypes = [[] if p1 is None or p2 is None else [p1.genotype, p2.genotype] for p1, p2 in parents]
        parents = [] if not parents else list(np.concatenate(parents).flat)

        # sort the individuals by rank and sparsity (sparsity according to already selected individuals)
        for idx, s in enumerate(individuals):
            s.others = [p for p in parents if p.genotype != s.genotype]  # individuals[:idx] + individuals[idx + 1:]

        individuals.sort(key=lambda x: (x.rank, -x.sparsity()), reverse=True)

        # create ranks with min_value and difference (the weight of probability)
        ranks = [min_value + (difference * i) for i in range(len(individuals))]
        sum_ranks = sum(ranks)
        ranks = [rank / sum_ranks for rank in ranks]
        # select 2 individuals according to weight, and return if couple not already selected
        p1, p2 = None, None
        for _ in range(len(individuals)):
            p1, p2 = np.random.choice(individuals, 2, p=ranks, replace=False)
            if [p1.genotype, p2.genotype] not in parents_genotypes and \
                    (not check_both_way or [p2.genotype, p1.genotype] not in parents_genotypes):
                return p1, p2
        return p1, p2

    @staticmethod
    def _tournament_selection(individuals, parents, check_both_way=False, X=5):
        """ select the 2 best individuals over X random individuals """
        random_indices = np.random.choice(len(individuals), X, replace=False)
        random_individuals = [individuals[i] for i in random_indices]
        parents_genotypes = [[] if p1 is None or p2 is None else [p1.genotype, p2.genotype] for p1, p2 in parents]
        parents = [] if not parents else list(np.concatenate(parents).flat)
        for idx, s in enumerate(random_individuals):
            s.others = [p for p in parents if p.genotype != s.genotype]  # individuals[:idx] + individuals[idx + 1:]

        # we sort the individuals by rank first, then by sparsity in case of equality
        random_individuals.sort(key=lambda x: (x.rank, -x.sparsity()))

        """
            we avoid to select an already selected couple of parents:
            E.g. with X=5; for individuals 0 and 1, we select them if not already existing
            otherwise we try 0,2; then 0,3; then 0,4; then 1,2; then 1,3; then 1,4; ... 
            Until 3,4. If all the couples already exists, we start a tournament_selection again
            For any number of individuals per generation, there will not be any infinite loop since
            the number of possibilities is always higher than the number of individuals to generate
        """
        i1 = 0
        i2 = 1
        while i1 <= X - 2 and i2 <= X - 1:
            if [random_individuals[i1].genotype, random_individuals[i2].genotype] not in parents_genotypes and \
                    (not check_both_way or
                     [random_individuals[i2].genotype, random_individuals[i1].genotype] not in parents_genotypes):
                # return the new couple. If check_both_way set to True, return them in a random order
                return (random_individuals[i1], random_individuals[i2]) \
                    if not check_both_way or random.random() < 0.5 else (random_individuals[i2], random_individuals[i1])
            i1 = i1 + 1 if i2 == X - 1 else i1
            i2 = i1 + 1 if i2 == X - 1 else i2 + 1
        return NeuralEvolutionaryArtStyleTransfer._tournament_selection(
            individuals, parents, check_both_way=check_both_way, X=X
        )

    @staticmethod
    def _select_random_parents(individuals, parents, all_different=False):
        """
            select a couple of random parents not already selected during the same generation
            if all_different set to True, all the selected parents will be new (not already selected in any couple)
        """
        if all_different:
            parents_genotypes = [p.genotype for p in ([] if not parents else list(np.concatenate(parents).flat))]
            filtered_individuals = [ind for ind in individuals if ind.genotype not in parents_genotypes]
            # if there are enough new individuals, we select them
            if len(filtered_individuals) >= 2:
                idx1, idx2 = random.sample(range(0, len(filtered_individuals)), 2)
                return filtered_individuals[idx1], filtered_individuals[idx2]

        def _couple_exists(i1, i2):
            """ return True if the couple of parents is already the couple of parents of one individual"""
            for p1, p2 in parents:
                if (p1.genotype == individuals[i1].genotype and
                    p2.genotype == individuals[i2].genotype) or \
                        (p2 == individuals[i2].genotype and
                         p1 == individuals[i1].genotype):
                    return True
            return False

        while True:
            idx1, idx2 = random.sample(range(0, len(individuals)), 2)
            if _couple_exists(idx1, idx2):
                continue
            return individuals[idx1], individuals[idx2]
        # idx1, idx2 = random.sample(range(0, len(individuals)), 2)
        # return individuals[idx1], individuals[idx2]

    @staticmethod
    def _cross_over_process(NST, mutation_manager, output_dir="", verbose=False):
        """
            create a cross-over image between a parent_content and a parent_style
            Run Neural Style Transfer with the parameters output_dir, tmp_dir, nb_epochs
        """
        # catch Ctrl + C in process
        signal.signal(signal.SIGINT, signal.SIG_IGN)

        name = NST.name
        individual_path = os.path.join(output_dir, f"{name}.jpg")

        # mutate before style transfer
        verbose_print(verbose, f"[Mutation] Pre-mutate cross-over {name}...")
        NST.content_weight, NST.style_weight = mutation_manager.mutate_before_style_transfer(
            individual_path, NST.content_weight, NST.style_weight, verbose=(verbose > 1)
        )

        # create the cross_over
        verbose_print(verbose, f"[Style Transfer] Create cross-over {name}...")
        _, _, result_path = NST.run()
        del NST

        # mutate after style transfer
        verbose_print(verbose, f"[Mutation] Post-mutate cross-over {name}...")
        mutation_manager.mutate_after_style_transfer(
            individual_path, verbose=(verbose > 1)
        )

    @staticmethod
    def _fitness_calculation(individuals, mutation_details, results_dir="", verbose=False):
        """ compute the fitness of the individuals, and record them in files """

        # set others
        for idx, individual in enumerate(individuals):
            individual.others = individuals[:idx] + individuals[idx + 1:]

        # apply ranks to all the individuals -> it will set phenotype if necessary
        NeuralEvolutionaryArtStyleTransfer._apply_rank(individuals)

        # save the results
        for idx, individual in enumerate(individuals):
            # compute creativity
            valuable = individual.value()
            novel = individual.novelty()
            surprising = individual.surprise()
            creative = individual.is_creative(V=valuable, N=novel, S=surprising)

            # write results to file
            filename = os.path.splitext(os.path.basename(individual.genotype))[0]
            full_path = os.path.join(results_dir, filename) + ".txt"

            content = f"IMAGE\n"
            content += f"name={filename}\n"
            content += f"content={'' if individual.parent1 is None else individual.parent1.genotype}\n"
            content += f"style={'' if individual.parent2 is None else individual.parent2.genotype}\n"

            content += f"\nSCORES\n"
            content += f"rank={individual.rank}\n"
            for idx_objective, score in enumerate(individual.objective_scores):
                content += f"{type(individual.objectives[idx_objective]()).__name__}={round(score, 6)}\n"

            content += f"\nMUTATIONS\n"
            for mutation_idx, details in enumerate(mutation_details[idx]):
                content += f"mutation_{mutation_idx + 1}=[{details}]\n"

            content += f"\nMETRICS\n"
            content += f"valuable={valuable}\n"
            content += f"novel={novel}\n"
            content += f"surprising={surprising}\n"
            content += f"creative={creative}\n"

            verbose_print(verbose, f"Write results for individual {individual.genotype}")
            try:
                f = open(full_path, "w")
                f.write(content)
                f.close()
            except IOError:
                print(f"Could not write file {full_path}")

    def _save_progress(self, save_file=None):
        """ get and save the actual state of objects """
        mutation_details = [] if self.mutation_manager is None else \
            [self.mutation_manager.details(img.genotype) for img in self.new_generation]
        steps = [self.current_generation]
        steps += self.done_generation
        selected_save_file = self.save_file if save_file is None else save_file
        SaveManager.save(
            self.individuals, self.new_generation, steps, mutation_details,
            self.save_dir, selected_save_file, self.selected_parents
        )

    def _create_individuals_from_selection_multiprocess(self, output_dir, tmp_dir, nb_epochs, selected_individuals,
                                                        size_pop, g, verbose, nb_processes=1):
        """
            Apply selection and cross over to create the new generation
            for each couple of selected parents, create 1 child
        """

        # init parents
        parents = []
        if self.initial_generation == g and self.continue_moea:
            parents = NeuralEvolutionaryArtStyleTransfer._get_parents(self.new_generation, return_genotypes=False)
        if not self.done_generation or not self.new_generation:
            # done_generation are True if individual already computed in the save
            self.done_generation = [False for _ in range(size_pop)]

        """
            Parent selection for all the new individuals using rank selection
            during the first generation, parents are selected randomly
        """
        selected_parents = {}
        for i in range(size_pop):
            if self.done_generation[i]:
                selected_parents[i] = []
            else:
                parent1, parent2 = self._select_random_parents(
                    selected_individuals, parents, self.unique
                ) if g == 0 else self._rank_selection(
                    selected_individuals, parents, check_both_way=True, min_value=self.selection_x
                )  # self._tournament_selection(selected_individuals, parents, check_both_way=True, X=self.selection_x)
                verbose_print(verbose, f"[Cross-over {i}] Parents selected: {parent1.genotype} AND {parent2.genotype}...")
                parents.append([parent1, parent2])
                selected_parents[i] = [parent1, parent2]

        """ initiate NST and Individual instance for all the cross-over """
        NSTs = {}
        idx_to_process = [idx for idx in range(size_pop) if not self.done_generation[idx]]
        individuals = {}
        for i in idx_to_process:
            NSTs[i] = NeuralStyleTransfer(selected_parents[i][0].genotype, selected_parents[i][1].genotype,
                                          output_path=output_dir, temporary_path=tmp_dir,
                                          num_iterations=nb_epochs, name=f"gen{g + 1}_ind{i}",
                                          verbose=verbose
                                          )
            verbose_print(verbose, f"Initiate {NSTs[i].name}...")
            path = os.path.join(output_dir, f"{NSTs[i].name}.jpg")
            individuals[i] = Individual(path, selected_parents[i][0], selected_parents[i][1], verbose=(verbose > 1))
            self.mutation_manager.init_mutations(path)


        """
            create all the new individuals using the NST/Individual instantiated
            nb_processes NST run at the same time and are joined afterwards
            once created, the individuals are added to the new generation
        """
        while len(idx_to_process) > 0:
            self.processes = []
            indexes = []
            for process_idx in range(nb_processes):
                if len(idx_to_process) == 0:
                    break
                current_idx = idx_to_process.pop(0)
                indexes.append(current_idx)
                self.processes.append(Process(
                    target=self._cross_over_process, args=(
                        NSTs[current_idx], self.mutation_manager, output_dir, verbose
                    )
                ))
            verbose_print(verbose, f"Create {len(self.processes)} process(es).")
            for process in self.processes:
                process.start()
            for process in self.processes:
                process.join()
            verbose_print(verbose, f"Processes terminated.")
            for idx in indexes:
                self.new_generation.append(individuals[idx])
                self.done_generation[idx] = True

    def _moea(self, output_dir, tmp_dir, results_dir, nb_generations, nb_epochs, max_individuals_per_gen, verbose, nb_processes=1):
        """ run the actual multi-objective algorithm """
        size_pop = min(max_individuals_per_gen, len(self.individuals))
        for g in range(self.initial_generation, nb_generations):
            # reset saved values
            self.current_generation = g
            self.current_cross_over = self.initial_idx_population
            self.mutation_manager = MutationManager(self.mutation_details)
            if g != self.initial_generation:
                self.new_generation = []

            # save current data between each generation
            save_file = f"{self.save_file}_gen{g}"
            if g == 0 or g != self.initial_generation:
                verbose_print(verbose, f"Data for the current generation are being saved in '{save_file}'.")
                self._save_progress(save_file)

            verbose_print(verbose, f"\n{'Continue' if self.continue_moea else 'Start'} generation {g + 1}...")
            """
                Selection
                    During the first generation, we do not select individuals
                    All the artworks are kept
            """
            selected_individuals = self.individuals if g == 0 else self._apply_rank(self.individuals)

            # Select the parents and create the individuals in self.new_generation
            self._create_individuals_from_selection_multiprocess(
                output_dir, tmp_dir, nb_epochs, selected_individuals, size_pop, g, verbose, nb_processes=nb_processes
            )

            # compute and log the fitness for the new individuals
            mutation_details = [self.mutation_manager.details(img.genotype) for img in self.new_generation]
            self._fitness_calculation(self.new_generation, mutation_details, results_dir, verbose=verbose)

            # normaly individuals = 25 from individuals + 25 from new
            # but set individuals = new_generation
            self.individuals = self.new_generation

            # reset saved values
            self.initial_idx_population = 0

        verbose_print(verbose, f"MOEA Terminated.")

    def run_moea(self, output_dir, tmp_dir, results_dir, nb_generations, nb_epochs,
                 max_individuals_per_gen, selection_x, verbose=False, inputs=None, unique=False, nb_processes=1):
        """
            Run the MOEA (selection, cross-over, mutation and start again)
            Selection using ranking & sparsity as NSGA-II

            :param inputs: paths of the first generation of artworks. MUST BE specified if not self.continue_moea
            :param output_dir: path of the directory where outputs will be stored
            :param tmp_dir: path of the directory where temporary files will be stored
            :param results_dir: path of the directory where metrics will be stored
            :param nb_generations: number of generation in the MOEA
            :param max_individuals_per_gen: maximum number of individuals per generation
            :param nb_epochs: number of Epochs for each NST
            :param selection_x: in tournament selection, select the 2 best individuals over 'selection_x' random individuals
            :param verbose: level of verbosity
            :param unique: The selected individuals are unique (selected only once) during the first generation.
            :param nb_processes: Number of processes used to run the cross-over (run different NST in parallel)
        """
        self.unique = unique
        self.selection_x = selection_x
        if self.continue_moea:
            self.individuals = self.first_individuals
        else:
            verbose_print(verbose, "Initiate the first individuals...")
            self.individuals = [Individual(i, verbose=(verbose > 1)) for i in inputs]

            # initiate the first generation and save them
            for individual in self.individuals:
                verbose_print(verbose, f"Compute phenotype for {individual.genotype}...")
                individual.phenotype()

        # if Ctrl+C pressed, the current state in saved and the program exited
        try:
            self._moea(output_dir, tmp_dir, results_dir, nb_generations,
                       nb_epochs, max_individuals_per_gen, verbose, nb_processes=nb_processes)
        except KeyboardInterrupt:
            for process in self.processes:
                process.terminate()
                process.join()
            full_path_save = os.path.join(self.save_dir, self.save_file)
            print(f"\nKeyboardInterrupt ! Current data are being saved in '{full_path_save}'.")
            self._save_progress()
