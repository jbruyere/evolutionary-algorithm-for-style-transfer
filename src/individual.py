import numpy as np
import os

from src.measures.color_distribution import ColorDistributionMeasure
from src.measures.complexity import ComplexityMeasure
from src.measures.liveliness import LivelinessMeasure
from src.measures.luminance_distribution import LuminanceDistributionMeasure
from src.measures.luminance_redundancy import LuminanceRedundancyMeasure


class Individual:
    """
    an individual in the MOEA

    CREATIVITY MEASURE (VALUE * NOVELTY * SURPRISE):
        VALUE
            each measure (objective) is a measure of aesthetic --> i.e., the value
            - Color distribution
            - Complexity
            - Liveliness
            - Luminance Distribution
            - Luminance Redundancy

        NOVELTY in the original dataset (the mean distance with the other individuals)
            MEAN  0.1835716611602007
            STD   0.07435019365334887
            MIN   0.12960804206959253
            Q1    0.13950070537000095
            Q2    0.158081805934828
            Q3    0.19323358258613102
            MAX   0.47806160776178636

        SURPRISE
            for each metric, create a range for parents until gen - X (maybe X = 2).
            The current value is expected to be in the range e.g., for metric M:
            grand-parent1=0.2, grand-parent2=0.3, grand-parent3=0.23, grand-parent4=0.33
            grand-parent5=0.27, grand-parent6=0.35, grand-parent7=0.16, grand-parent7=0.28
            parent1=0.22, parent2=0.34
            So M for child is expected to be 0.16 <= M <= 0.35
            If at least one M out of all the metrics is outside the ranges ==> SURPRISE
    """
    novelty_threshold = 0.16
    surprise_generations = 2

    def __init__(self, img_path, parent1=None, parent2=None, verbose=False):
        self.genotype = img_path
        self.objectives = [
            ColorDistributionMeasure,
            ComplexityMeasure,
            LivelinessMeasure,
            LuminanceDistributionMeasure,
            LuminanceRedundancyMeasure
        ]
        self.objective_scores = [0 for _ in self.objectives]
        self.others = []
        self.parent1 = parent1
        self.parent2 = parent2
        self.rank = 0
        self.verbose = verbose

    def phenotype(self):
        """ compute the objectives of the individual defined by its genotype (its image) """
        if self.verbose:
            print(f"Compute metrics for individual {self.genotype}")
        self.objective_scores = [objective(verbose=self.verbose).apply(self.genotype) for objective in self.objectives]
        return self.objective_scores

    def dominated_by(self, other):
        """ individual is dominated by another if the other has one objective > and all other >= """
        return sum([1 if self.objective_scores[i] <= other.objective_scores[i] else 0
                    for i in range(len(self.objective_scores))]) == len(self.objective_scores) and \
               sum(self.objective_scores) < sum(other.objective_scores)

    def __lt__(self, other):
        """ require to sort the individuals (individual lower is dominated by a one bigger) """
        return self.dominated_by(other)

    def dominated(self, others):
        """ return True if the individual is dominated by at least one in others """
        for other in others:
            if self.dominated_by(other):
                return True
        return False

    @staticmethod
    def _get_ancestors(individual, nb_generation=0, nb_generation_max=50):
        """
            recursively return the ancestors of the individual
            (stop when nb_generation >= nb_generation, or when there is no more ancestor)
        """
        if individual is None or nb_generation >= nb_generation_max:
            return []
        ancestors_parent1 = Individual._get_ancestors(individual.parent1, nb_generation + 1, nb_generation_max)
        ancestors_parent2 = Individual._get_ancestors(individual.parent2, nb_generation + 1, nb_generation_max)
        ancestors = ancestors_parent1 + ancestors_parent2
        if individual.parent1 is not None:
            ancestors.append(individual.parent1)
        if individual.parent2 is not None:
            ancestors.append(individual.parent2)
        return ancestors

    def sparsity(self, others=None, recursive=False):
        """ compute the mean distance between the individual and the others """
        distance = 0
        others = self.others if others is None else others

        # if recursive get all the ancestors and make them unique in the list others
        if recursive:
            ancestors = [Individual._get_ancestors(individual) for individual in others]
            others = [] if all([not x for x in ancestors]) else list(set(np.concatenate(ancestors).flat))

        for other in others:
            distance += np.sqrt(sum([
                np.square(self.objective_scores[i] - other.objective_scores[i]) for i in
                range(len(self.objective_scores))
            ]))
        return 0 if len(others) == 0 else distance / len(others)

    def value(self) -> int:
        """ individual is considered valuable (1) if all the objectives are above their threshold (else O) """
        for i, score in enumerate(self.objective_scores):
            if score < self.objectives[i].min_valuable():
                return 0
        return 1

    def value_score(self) -> float:
        score = 1.0
        for s in self.objective_scores:
            score = score * s
        return score

    def novelty(self) -> int:
        """
            compute the novelty (1 or 0) according to the distance
            between the individual and all the others (including the ancestors)
        """
        sparsity = self.sparsity(recursive=True)
        return 1 if sparsity > Individual.novelty_threshold else 0

    def novelty_score(self) -> float:
        return self.sparsity(recursive=True)

    def surprise(self) -> int:
        """
            if at least one score of the individual is outside the expected range
            (based on parents scores), then it is surprising and returns 1 (0 otherwise)
        """
        ancestors = Individual._get_ancestors(self, 0, Individual.surprise_generations)
        if len(ancestors) == 0:
            return 1

        range_min = [
            min(ancestors, key=lambda a: a.objective_scores[i]).objective_scores[i] for i in range(len(self.objectives))
        ]
        range_max = [
            max(ancestors, key=lambda a: a.objective_scores[i]).objective_scores[i] for i in range(len(self.objectives))
        ]

        for i, score in enumerate(self.objective_scores):
            if not range_min[i] <= score <= range_max[i]:
                return 1
        return 0

    def surprise_score(self) -> float:
        ancestors = Individual._get_ancestors(self, 0, Individual.surprise_generations)
        if len(ancestors) == 0:
            return 1.0

        range_min, range_max = [
            min(ancestors, key=lambda a: a.objective_scores[i]).objective_scores[i] for i in range(len(self.objectives))
        ], [
            max(ancestors, key=lambda a: a.objective_scores[i]).objective_scores[i] for i in range(len(self.objectives))
        ]
        return sum([1 / len(self.objective_scores) if not range_min[i] <= score <= range_max[i] else 0.0 for i, score in enumerate(self.objective_scores)])

    def is_creative(self, V=None, N=None, S=None) -> bool:
        """ a creative individual should be valuable, novel and surprising """
        value = V if V is not None else self.value()
        novel = N if N is not None else self.novelty()
        surprise = S if S is not None else self.surprise()
        return True if value * novel * surprise > 0 else False

    def save(self, directory_path="", recursive=False, erase_if_exists=False) -> bool:
        """
            save the current individual
            :param directory_path: directory where the file will be created
            :param recursive: also write the parents and parents of parents etc...
            :param erase_if_exists: overwrite the file if it already exists
            :return True if everything OK, False if the file does not exist
        """
        get_name = lambda x: os.path.splitext(os.path.basename(x))[0]
        full_path = os.path.join(directory_path, get_name(self.genotype))
        if not erase_if_exists and os.path.exists(full_path):
            return False

        content = ""
        content += f"genotype={self.genotype}\n"
        content += f"rank={self.rank}\n"
        content += f"parent1={'' if self.parent1 is None else get_name(self.parent1.genotype)}\n"
        content += f"parent2={'' if self.parent2 is None else get_name(self.parent2.genotype)}\n"
        content += f"scores={','.join([str(score) for score in self.objective_scores])}\n"

        f = open(full_path, "w")
        f.write(content)
        f.close()

        if recursive and self.parent1 is not None:
            self.parent1.save(directory_path, recursive, erase_if_exists)
        if recursive and self.parent2 is not None:
            self.parent2.save(directory_path, recursive, erase_if_exists)
        return True

    @staticmethod
    def load(full_path, recursive=False, verbose=False):
        """
            Load an individual from a file, and associated parents if recursive
            :param full_path: file to load
            :param recursive: load the associated parent files recursively
            :param verbose: level of verbosity
            :return Individual
        """
        directory = os.path.dirname(full_path)
        f = open(full_path, "r")
        content = [line.split("=") for line in f.read().split('\n') if line != '']
        f.close()

        data_line = lambda full_content, name: next(line for line in full_content if line[0] == name)[1]
        genotype = data_line(content, "genotype")
        parent1 = data_line(content, "parent1")
        parent2 = data_line(content, "parent2")

        parent1 = None if (parent1 == '' or recursive is False)\
            else Individual.load(os.path.join(directory, parent1), recursive=recursive, verbose=verbose)
        parent2 = None if (parent2 == '' or recursive is False)\
            else Individual.load(os.path.join(directory, parent2), recursive=recursive, verbose=verbose)

        individual = Individual(genotype, parent1, parent2, verbose)
        individual.rank = int(data_line(content, "rank"))
        individual.objective_scores = [float(value) for value in data_line(content, "scores").split(',')]

        return individual
