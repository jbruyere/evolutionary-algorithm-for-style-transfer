import os
from src.individual import Individual


class SaveManager:
    @staticmethod
    def can_restore_save(save_file) -> bool:
        return os.path.exists(save_file) and os.path.isfile(save_file)

    @staticmethod
    def _retrieve_data_line(content, name):
        next_data = next((line for line in content if line[0] == name), None)
        if next_data is not None:
            return next_data[1].split(',')
        return None

    @staticmethod
    def _retrieve_data_line_mutation(content, idx):
        return [m for m in ("=".join(next(line for line in content if line[0] == f"mutations_{idx}")[1:])).split("|||") if m != '']

    @staticmethod
    def str2bool(str):
        return str in ['True', 'true', 't', 'T']

    @staticmethod
    def load(save_dir, save_file, verbose=False):
        f = open(os.path.join(save_dir, save_file), 'r')
        content = [line for line in f.read().split('\n') if line != '']
        f.close()

        content = [line.split('=') for line in content]

        # "individuals=path1,path2,path3"
        individuals_path = [os.path.join(save_dir, path) for path in SaveManager._retrieve_data_line(content, "individuals")]
        individuals = [Individual.load(path, recursive=True, verbose=verbose) for path in individuals_path]

        selected_parents = SaveManager._retrieve_data_line(content, "selected_parents")
        if selected_parents is not None:
            selected_parents = [Individual.load(os.path.join(save_dir, p), recursive=True) for p in selected_parents]

        # "new_generation=path1,path2,path3"
        new_generation_path = [os.path.join(save_dir, path) for path in SaveManager._retrieve_data_line(content, "new_generation") if path != '']
        new_generation = [Individual.load(path, recursive=True, verbose=verbose) for path in new_generation_path]

        # "steps=int,int,bool,bool
        retrieved_steps = SaveManager._retrieve_data_line(content, "steps")
        steps = [int(retrieved_steps[0])]
        steps += [SaveManager.str2bool(retrieved_steps[step]) for step in range(1, len(retrieved_steps))]

        # "mutations_0=mutation0A|||mutation0B"
        # "mutations_1=mutation1A|||mutations1B|||mutation1C"
        mutation_details = {}
        for idx, individual in enumerate(new_generation):
            mutation_details[individual.genotype] = SaveManager._retrieve_data_line_mutation(content, idx)

        return individuals, new_generation, mutation_details, steps, selected_parents

    @staticmethod
    def save(individuals, new_generation, steps, mutation_details, save_dir, save_file, selected_parents=None):

        for individual in new_generation:
            individual.save(save_dir, recursive=True, erase_if_exists=True)
        for individual in individuals:
            individual.save(save_dir, recursive=True, erase_if_exists=False)

        content = ""
        content += f"individuals={','.join([os.path.splitext(os.path.basename(individual.genotype))[0] for individual in individuals])}\n"
        content += f"new_generation={','.join([os.path.splitext(os.path.basename(individual.genotype))[0] for individual in new_generation])}\n"
        content += f"steps={','.join([str(i) for i in steps])}\n"
        if selected_parents is not None:
            content += f"selected_parents={','.join([os.path.splitext(os.path.basename(individual.genotype))[0] for individual in selected_parents])}\n"

        for i in range(len(new_generation)):
            content += f"mutations_{i}={'|||'.join(mutation_details[i])}\n"

        f = open(os.path.join(save_dir, save_file), 'w')
        f.write(content)
        f.close()