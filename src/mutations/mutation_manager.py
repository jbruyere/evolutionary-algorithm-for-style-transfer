import random
from src.mutations.mutation import Mutation, ChangeWeightMutation, ColorMutation, MixWithGeneratedImageMutation


class MutationManager:
    """
        Randomly creates between 0 and 3 mutations to apply, and apply them when necessary
        mutation -> not 100% of the time
        50%-> no mutation, 30%-> 1 mutation, 15%-> 2 mutations, 5%-> 3 mutations
        mutations will lead to new changes in the paintings, so probably new metrics values
    """
    def __init__(self, images_mutations_details=None):
        self.images_mutations = {}
        self.images_mutations_details = {} if images_mutations_details is None else images_mutations_details
        self.nb_of_mutations_ratio = {
            1: 0.3,   # 1 mutation is created 30% of the time
            2: 0.15,  # 2 mutations are created 15% of the time
            3: 0.05   # 3 mutations are created 5% of the time
        }
        # when creating a mutation, it is created regarding those ratio
        self.mutations_ratio = {
            ChangeWeightMutation: 1/3,
            ColorMutation: 1/3,
            MixWithGeneratedImageMutation: 1/3
        }

    def _random_mutation(self, excluded=None):
        """ select on mutation among the mutations according to their ratio """
        """ can exclude certain types to do not create twice the same """

        # we create a new dictionary of mutations ratio where excluded are not present
        ratio = self.mutations_ratio.copy()
        types = self.mutations_ratio.keys()
        for t in types:
            if excluded is not None and any([e for e in excluded if type(e) == t]):
                ratio.pop(t, None)  # remove the type from the dictionary
        range_values = sum(list(ratio.values()))
        # return if ratio is empty
        if not ratio:
            return None

        # generate a value in the range of included mutations ratio
        random_value = random.random() * range_values

        # select a mutation according to the random_value
        count = 0
        for mutation in ratio.keys():
            if count <= random_value < count + ratio[mutation]:
                return mutation()
            count += ratio[mutation]
        return None  # should not go there

    def _add_mutations(self, img_path, nb_of_mutations):
        """ add X mutations where X=nb_of_mutations. Add different mutations only"""
        mutations = []
        for _ in range(nb_of_mutations):
            mutation = self._random_mutation(excluded=mutations)
            if mutation is not None:
                mutations.append(mutation)
        self.images_mutations[img_path] = mutations

    def init_mutations(self, img_path):
        """ create mutations for individual img_path """
        random_value = random.random()
        nb_of_mutations = 0

        # select among the nb_of_mutations_ratio how many mutations to apply
        count = 0
        for nb in self.nb_of_mutations_ratio.keys():
            if count <= random_value < count + self.nb_of_mutations_ratio[nb]:
                nb_of_mutations = nb
                break
            count += self.nb_of_mutations_ratio[nb]

        # add the corresponding number of mutations
        self._add_mutations(img_path, nb_of_mutations)

    def details(self, img_path):
        """
            return the details of the mutations regarding img_path
            details could have been saved in images_mutations_details
        """
        if img_path in self.images_mutations:
            return [m.details() for m in self.images_mutations[img_path]]
        elif img_path in self.images_mutations_details:
            return self.images_mutations_details[img_path]
        else:
            return []

    def mutate_before_style_transfer(self, img_path, content_weight, style_weight, verbose=False):
        """ apply the registered mutation with an application 'Mutation.MUTATE_STYLE_TRANSFER' """
        for mutation in self.images_mutations[img_path]:
            if mutation.application() == Mutation.Application.MUTATE_STYLE_TRANSFER:
                if verbose:
                    print(f"Applying mutation {mutation.details()} to image {img_path} (before style-transfer)")
                content_weight, style_weight = mutation.mutate(
                    img_path=img_path,
                    content_weight=content_weight,
                    style_weight=style_weight
                )
        return content_weight, style_weight

    def mutate_after_style_transfer(self, img_path, verbose=False):
        """ apply the registered mutation with an application 'Mutation.MUTATE_IMAGE' """
        if img_path not in self.images_mutations:
            return img_path
        for mutation in self.images_mutations[img_path]:
            if mutation.application() == Mutation.Application.MUTATE_IMAGE:
                if verbose:
                    print(f"Applying mutation {mutation.details()} to image {img_path} (after style-transfer)")
                mutation.mutate(
                    img_path=img_path
                )
        return img_path

    def clear(self, img_path):
        """ remove img_path from the dictionary of images if exists"""
        self.images_mutations.pop(img_path, None)