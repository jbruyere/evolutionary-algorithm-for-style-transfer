from scipy.interpolate import UnivariateSpline
import numpy as np
import cv2

# FROM
# https://subscription.packtpub.com/book/application_development/9781785282690/1/ch01lvl1sec11/generating-a-warming-cooling-filter


class ColorFilter:
    def _create_LUT_8UC1(self, x, y):
        spl = UnivariateSpline(x, y)
        return spl(range(256))

    def __init__(self):
        self.incr_ch_lut = self._create_LUT_8UC1([0, 64, 128, 192, 256], [0, 70, 140, 210, 256])
        self.decr_ch_lut = self._create_LUT_8UC1([0, 64, 128, 192, 256], [0, 58,  116, 174, 232])
        self.revert = self._create_LUT_8UC1([0, 64, 128, 192, 256], [256, 192, 128, 64, 0])

    def apply(self, c_r, c_g, c_b):
        return c_r, c_g, c_b

    def render(self, img_rgb):
        c_r, c_g, c_b = cv2.split(img_rgb)
        c_r, c_g, c_b = self.apply(c_r, c_g, c_b)
        img_rgb = cv2.merge((c_r, c_g, c_b))

        # decrease color saturation
        c_h, c_s, c_v = cv2.split(cv2.cvtColor(img_rgb, cv2.COLOR_RGB2HSV))
        c_s = cv2.LUT(c_s, self.decr_ch_lut).astype(np.uint8)
        return cv2.cvtColor(cv2.merge((c_h, c_s, c_v)), cv2.COLOR_HSV2RGB)


class CoolingFilter(ColorFilter):
    def apply(self, c_r, c_g, c_b):
        c_r = cv2.LUT(c_r, self.decr_ch_lut).astype(np.uint8)
        c_b = cv2.LUT(c_b, self.incr_ch_lut).astype(np.uint8)
        return c_r, c_g, c_b


class WarmingFilter(ColorFilter):
    def apply(self, c_r, c_g, c_b):
        c_r = cv2.LUT(c_r, self.incr_ch_lut).astype(np.uint8)
        c_b = cv2.LUT(c_b, self.decr_ch_lut).astype(np.uint8)
        return c_r, c_g, c_b


class BrightnessFilter(ColorFilter):
    def apply(self, c_r, c_g, c_b):
        c_r = cv2.LUT(c_r, self.incr_ch_lut).astype(np.uint8)
        c_g = cv2.LUT(c_g, self.incr_ch_lut).astype(np.uint8)
        c_b = cv2.LUT(c_b, self.incr_ch_lut).astype(np.uint8)
        return c_r, c_g, c_b


class DarknessFilter(ColorFilter):
    def apply(self, c_r, c_g, c_b):
        c_r = cv2.LUT(c_r, self.decr_ch_lut).astype(np.uint8)
        c_g = cv2.LUT(c_g, self.decr_ch_lut).astype(np.uint8)
        c_b = cv2.LUT(c_b, self.decr_ch_lut).astype(np.uint8)
        return c_r, c_g, c_b