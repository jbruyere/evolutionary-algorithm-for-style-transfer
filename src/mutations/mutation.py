import random
import numpy as np
from PIL import Image
import os
from enum import Enum

from src.mutations.color_filters import WarmingFilter, CoolingFilter, BrightnessFilter, DarknessFilter
from src.mutations.cppn import generate_cppn_image
from src.nst import NeuralStyleTransfer

# Hyperparameters
NUM_ITERATIONS_MIX_CPPN=15  # voluntarily small to do not affect to much the painting
STYLE_WEIGHT_MIX_CPPN=1e-4  # voluntarily small to do not affect to much the painting


class Mutation:
    """
        define a mutation to apply to an image
    """

    class Application(Enum):
        # is applied before the style transfer
        MUTATE_STYLE_TRANSFER = 0
        # is applied after the style transfer
        MUTATE_IMAGE = 1

    @staticmethod
    def application() -> int:
        """ return the type of mutation (MUTATE_STYLE_TRANSFER, MUTATE_IMAGE...) """
        pass

    def mutate(self, **kwargs):
        """ apply the mutation """
        pass

    def details(self) -> str:
        """ return a string explaining the mutation"""
        pass


class ChangeWeightMutation(Mutation):
    """
        Mutation that randomly changes the weights (content weight and style weight) used in the NST
    """

    def __init__(self):
        self.content_weight = random.choice([500, 750, 1000, 1250, 1500])
        self.style_weight = random.choice([5e-3, 10e-2, 15e-2, 20e-2, 25e-2])

    @staticmethod
    def application() -> Mutation.Application:
        return Mutation.Application.MUTATE_STYLE_TRANSFER

    def mutate(self, **kwargs):
        """ :return: content_weight, style_weight """
        return self.content_weight, self.style_weight

    def details(self) -> str:
        return f"content_weight={self.content_weight},style_weight={self.style_weight}"


class ColorMutation(Mutation):
    """
        Mutation that applies a random ColorFilter on the image
    """
    possibilities = {
        "cool": CoolingFilter,
        "warm": WarmingFilter,
        "bright": BrightnessFilter,
        "dark": DarknessFilter
    }

    def __init__(self):
        self.choice = random.choice(list(ColorMutation.possibilities.keys()))

    @staticmethod
    def application() -> Mutation.Application:
        return Mutation.Application.MUTATE_IMAGE

    def mutate(self, **kwargs):
        img_path = kwargs["img_path"]
        image = Image.open(img_path)
        img = np.array(image)
        image.close()
        mutated_img = ColorMutation.possibilities[self.choice]().render(img)
        Image.fromarray(mutated_img).save(img_path)

    def details(self) -> str:
        return f"color_filter={self.choice}"


class MixWithGeneratedImageMutation(Mutation):
    """
        Mutation that generates an image with CPPN algorithm.
        It replace the image by a mix with the generated one (using NST)
    """

    def __init__(self):
        self.style_dir = "dataset/tmp_images"
        self.style_path = os.path.join(self.style_dir, generate_cppn_image(directory_path=self.style_dir))
        # voluntarily small to do not affect to much the painting
        self.num_iterations = NUM_ITERATIONS_MIX_CPPN
        # voluntarily small to do not affect to much the painting
        self.style_weight = STYLE_WEIGHT_MIX_CPPN

    @staticmethod
    def application() -> Mutation.Application:
        return Mutation.Application.MUTATE_IMAGE

    def mutate(self, **kwargs):
        content_path = kwargs["img_path"]
        filename = os.path.splitext(os.path.basename(content_path))[0]
        dirname = os.path.dirname(content_path)
        nst = NeuralStyleTransfer(
            content_path,
            self.style_path,
            num_iterations=self.num_iterations,
            style_weight=self.style_weight,
            name=filename,
            output_path=dirname
        )
        nst.run()
        del nst

    def details(self) -> str:
        return f"style_transfer={os.path.join(self.style_dir, self.style_path)}"
