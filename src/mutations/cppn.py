import torch
import torch.nn as nn
import numpy as np
import matplotlib.pyplot as plt
import os


# This code is inspired from https://github.com/zavalit/cppn


def _init_normal(m):
    if type(m) == nn.Linear:
        nn.init.normal_(m.weight)


class NN(nn.Module):

    def __init__(self, activation=nn.Tanh, num_neurons=16, num_layers=10):
        super(NN, self).__init__()
        layers = [nn.Linear(2, num_neurons, bias=True), activation()]
        for _ in range(num_layers - 1):
            layers += [nn.Linear(num_neurons, num_neurons, bias=False), activation()]
        layers += [nn.Linear(num_neurons, 3, bias=False), nn.Sigmoid()]
        self.layers = nn.Sequential(*layers)

    def forward(self, pixel):
        return self.layers(pixel)


def _plot_colors(colors, fig_size=4):
    plt.figure(figsize=(fig_size, fig_size))
    plt.imshow(colors, interpolation='nearest', vmin=0, vmax=1)


def _save_colors(colors, img_format='png', path='', verbose=False):
    filename = str(np.random.randint(100000)) + "." + img_format
    if verbose:
        print("save to file %s" % filename)
    plt.imsave(os.path.join(path, filename), colors, format=img_format)
    return filename


def generate_cppn_image(neurons=16, layers=10, size=512, img_format='jpg', verbose=False, directory_path=''):
    if verbose:
        print("create image of %s neurons %s layers of size %sx%s" % (neurons, layers, size, size))
    net = NN(num_neurons=neurons, num_layers=layers)
    net.apply(_init_normal)
    x = np.arange(0, size, 1)
    y = np.arange(0, size, 1)
    colors = np.zeros((size, size, 2))
    for i in x:
        for j in y:
            colors[i][j] = np.array([float(i) / size - 0.5, float(j) / size + 0.5])
    colors = colors.reshape((size * size, 2))
    img = net(torch.tensor(colors).type(torch.FloatTensor)).detach().numpy()
    img = img.reshape(size, size, 3)
    del net
    return _save_colors(img, img_format, path=directory_path, verbose=verbose)
