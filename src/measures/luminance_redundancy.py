import numpy as np
from PIL import Image
from src.measures.aesthetic_measure import AestheticMeasure

"""
from

Rigau, J., Feixas, M., and Sbert, M. (2007).
Conceptualizing Birkhoff’s Aesthetic Mea-sure Using Shannon Entropy and Kolmogorov Complexity.
In Cunningham, D. W.,Meyer, G., and Neumann, L., editors,Computational Aesthetics in Graphics, Visual-ization, and Imaging. The Eurographics Association.
"""


class LuminanceRedundancyMeasure(AestheticMeasure):
    """
        use Shannon perspective, calculate the redundancy of the luminance
        bigger number means redundant palette
        number near 0 means complex palette (considered better) -> so 1 - result
    """
    def __init__(self, verbose=False):
        self.verbose = verbose

    @staticmethod
    def min_valuable() -> float:
        return 0.95

    def apply(self, img_path) -> float:
        pixels = AestheticMeasure.open_PIL_image(img_path)
        pixels_luminance = [AestheticMeasure.luminance(pixel) for line in pixels for pixel in line]
        unique_luminance = set(pixels_luminance)

        H = np.log2(256)
        Hp = np.log2(len(unique_luminance))

        M = (H - Hp) / H
        M = 1 - M

        if self.verbose:
            print(f"[Measure] Shannon measure (luminance redundancy): {M}")

        return M
