import numpy as np
from PIL import Image
import math
from src.measures.aesthetic_measure import AestheticMeasure


class ComplexityMeasure(AestheticMeasure):
    """
        own interpretation of complexity
        compute the complexity of the image
        based on; order <-> complexity <-> disorder
        measure on far the pixels are from each others (how far they are from the neighbours - in term of colors)
        we ignore the pixels on first/last row/column

        Results:
        random images (random noise) are around 169-170
        full-red (or full-green, full-blue, etc...) pictures are = 0

        Images generated with CPPN are around 5-10 -> quite ordered but not so much

        Artworks (with the dataset of 50):
        MIN:              5.523645248515307 (via-san-leonardo.jpg -> simple one)
        Q1:               12.467135061992044
        Q2 (MEDIANE):     16.81353058256038
        Q3:               22.51189663407262
        MAX:              41.961021437389945 (prisoners-exercising-prisoners-round-1890.jpg -> many spots)
        MEAN:             17.923001778511722
        STD:              8.091551728873045

        Abstract artworks are usually near 5 (because they are simple)
        Portraits are around 10 (complex but really ordered !)
        Impresionism higher ~~20 (because many spots and not precise lines)

        Let's say we ideally want 17 !

        Our measure is:
          1 - (abs(Ve - Vo) / Vw)
        Ve = expected value, Vo = obtained value, W = worst value
        = 1 - (abs(17 - Vo) / 170)

        Close to 1 -> perfect
        Close to 0 -> horrible
    """
    def __init__(self, verbose=False):
        self.verbose = verbose

    @staticmethod
    def min_valuable() -> float:
        return 0.85

    def apply(self, img_path) -> float:
        arr = AestheticMeasure.open_PIL_image(img_path)

        # 512 x 512
        size_y = len(arr)
        size_x = len(arr[0])

        distance = 0
        for j in range(1, size_y - 1):
            for i in range(1, size_x - 1):
                distanceAround = 0
                # For all the pixel around the current pixels, get the distance with the current pixel
                for pixel in [arr[j, i - 1], arr[j, i + 1], arr[j - 1, i], arr[j + 1, i], arr[j + 1, i + 1],
                              arr[j - 1, i + 1], arr[j - 1, i - 1], arr[j + 1, i - 1]]:
                    distR = (AestheticMeasure.red(arr[j][i]) - AestheticMeasure.red(pixel)) ** 2
                    distG = (AestheticMeasure.green(arr[j][i]) - AestheticMeasure.green(pixel)) ** 2
                    distB = (AestheticMeasure.blue(arr[j][i]) - AestheticMeasure.blue(pixel)) ** 2
                    distanceAround += math.sqrt(distR + distG + distB)
                distance += distanceAround / 8  # divide by the number of neighours to get a mean

        # number of pixel computed (all the pixels - the edges
        computedPixels = size_y * size_x - ((size_y * 2) + (size_x * 2) - 4)

        # mean distance
        value = distance / computedPixels

        # best possible value
        value_expected = 17
        # worst possible value (random images)
        value_worst = 170

        # 1 - (abs(Ve - Vo) / Vw)
        result = 1 - (abs(value_expected - value) / value_worst)

        if self.verbose:
            print(f"[Measure] Complexity measure (own measure): {result}")
        return result
