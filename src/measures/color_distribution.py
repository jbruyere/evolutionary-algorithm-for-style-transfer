import numpy as np
from PIL import Image
import math
from src.measures.aesthetic_measure import AestheticMeasure, Color

"""
inspired by
        B. J. Ross, W. Ralph and Hai Zong, "Evolutionary Image Synthesis Using a Model of Aesthetics,"
        2006 IEEE International Conference on Evolutionary Computation, 2006, pp. 1087-1094, doi: 10.1109/CEC.2006.1688430.
        https://ieeexplore.ieee.org/document/1688430
"""


def diagonal_length(pixels):
    """ size of the diagonal in the images """
    return math.sqrt(len(pixels) ** 2 + len(pixels[0]) ** 2)


def image_color_gradient(pixels, i, j, color, d):
    """
                             (R(i,j) - R(i+1,j+1))**2 + (R(i+1,j) - R(i,j+1))**2
        |diff R(i,j)| ** 2 = ---------------------------------------------------
                                                    d ** 2
    """
    return  ((int(pixels[i][j][color]) - int(pixels[i + 1][j + 1][color])) ** 2 +
            (int(pixels[i + 1][j][color]) - int(pixels[i][j + 1][color])) ** 2) / (d ** 2)


def get_gradient_stimulus(pixels):
    """
        S(i,j) = sqrt( |diff R(i,j)| ** 2 + |diff G(i,j)| ** 2 + |diff B(i,j)| ** 2)
    """
    size_x = len(pixels)
    size_y = len(pixels[0])
    S = np.zeros((size_x, size_y))
    d = 0.05 * diagonal_length(pixels)
    for i in range(size_x):
        for j in range(size_y):
            if i == 0 or j == 0 or i == size_x - 1 or j == size_y - 1:
                S[i][j] = -np.Inf
            else:
                S[i][j] = math.sqrt(
                    image_color_gradient(pixels, i, j, Color.R, d) +
                    image_color_gradient(pixels, i, j, Color.G, d) +
                    image_color_gradient(pixels, i, j, Color.B, d)
                )
    return S


def get_gradient_responses(pixels):
    """
        Response(i, j) = log(S(i,j) / S0)
    """
    responses = get_gradient_stimulus(pixels)
    responses = responses[responses != -np.Inf]
    responses = responses[responses != 0]
    S0 = 2
    responses = np.log(responses / S0)
    return responses


def weighted_mean(values):
    """ distribution mean """
    if len(values) == 0:
        return 0
    return sum(values ** 2) / sum(values)


def weighted_std(values, mean):
    """ distribution standard deviation """
    if len(values) == 0:
        return 0
    return math.sqrt(abs(sum([v * ((v - mean) ** 2) for v in values]) / sum(values)))


class ColorDistributionMeasure(AestheticMeasure):
    """
        color distribution using Ralph's bell curve

        for nomalized score around the values to reached (3 - 0.75)

        mean = abs(3 - mean) / 3
        std = abs(0.75 - std) / 0.75
        global_score = (mean + std) / 2

        global score near 0 -> good
        global score near 1 -> bad
        so we take (1 - score)
    """
    def __init__(self, compute_DFN=False, verbose=False):
        self.compute_DFN = compute_DFN
        self.verbose = verbose

    @staticmethod
    def min_valuable() -> float:
        return 0.3

    def apply(self, img_path) -> float:
        pixels = AestheticMeasure.open_PIL_image(img_path)
        # color 0-255 or 0-1 ? -> change the result in np.log
        R = get_gradient_responses(pixels)

        # WARNING : we take the absolute value that is not is the original formula but makes sense (since log qui negative values)
        mean = weighted_mean(R)
        abs_mean = abs(mean)
        std = weighted_std(R, mean)

        # we ideally want MEAN=3 and STD=0.75, so we compute a difference around those values
        mean = abs(3 - float(abs_mean)) / 3
        std = abs(0.75 - float(std)) / 0.75
        bellcurve_value = 1 - ((mean + std) / 2)

        if self.verbose:
            # print(f"Ralph's mean value: {abs_mean}")
            # print(f"Ralph's std value: {std}")
            print(f"[Measure] Final Bell curve value: {bellcurve_value}")

        return bellcurve_value


