import numpy as np
from PIL import Image
from src.measures.aesthetic_measure import AestheticMeasure


class LivelinessMeasure(AestheticMeasure):
    """
        from

        Eelco den Heijer
        Evolving Art using Measures for Symmetry, Compositional Balance and Liveliness
        https://www.scitepress.org/Papers/2012/41496/41496.pdf

        compute liveliness, i.e. the entropy of the intensity of the pixels of the image
        Images that are very monotonous will little
        variation in the intensity of the pixels and will have
        low entropy, and images with a lot of different intensity values will have high entropy
               Needs all the images to be the exact same size
               Complex paintings have smallest score and vice-versa
        e.g., abstract paintings with only scared -> high score
        e.g., Mona lisa & backgrounds -> low score
        maybe take (1 - score)
    """
    def __init__(self, verbose=False):
        self.verbose = verbose

    @staticmethod
    def min_valuable() -> float:
        return 0.93

    def apply(self, img_path) -> float:
        pixels = AestheticMeasure.open_PIL_image(img_path)
        pixels_intensity = [AestheticMeasure.intensity(pixel) for line in pixels for pixel in line]
        unique_intensity = set(pixels_intensity)
        p = {}

        for l in unique_intensity:
            p[l] = np.count_nonzero(np.array(pixels_intensity) == l) / len(pixels_intensity)

        # OK to divide by the number of pixels since all the images have the same number of pixels
        M_liveliness = 1 - (- sum([p[i] * np.log(p[i]) for i in pixels_intensity]) / len(pixels_intensity))

        if self.verbose:
            print(f"[Measure] Liveliness measure (the entropy of the intensity): {M_liveliness}")

        return M_liveliness