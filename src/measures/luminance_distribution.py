import matplotlib.pyplot as plt
import cv2
from src.measures.aesthetic_measure import AestheticMeasure


"""
from

den Heijer,  E. and Eiben,  A. E. (2010). 
Using aesthetic measures to evolve art.
InIEEE Congress on Evolutionary Computation, pages 1–8.

"""

def benfort_distribution():
    return [0, 0.301, 0.176, 0.125, 0.097, 0.079, 0.067, 0.058, 0.051, 0.046]


def H_benfort(i):
    return benfort_distribution()[i]


def H_image(b, i):
    return b[i - 1]


def d_max(p):
    return (1 - H_benfort(1)) ** p + sum([H_benfort(i) ** p for i in range(2, 10)])


def d_total(b, p, N):
    return sum([abs((H_image(b, i) / N) - H_benfort(i)) ** p for i in range(1, 10)])


def M_benfort(b, p, N):
    return (d_max(p) - d_total(b, p, N)) / (d_max(p))


class LuminanceDistributionMeasure(AestheticMeasure):
    """
        Benford Law (or first-digit law) states that list of numbers obtained from
        real life (i.e. not created by man) are distributed in a specific, non-uniform way
        -> measure the distribution of luminance (brightness) of pixels
        1 -> perfect
        0 -> horrible
    """
    def __init__(self, p=1, verbose=False):
        """
            :param p: Lower values for p (we experimented with p = 3, p = 2 and
            p = 1) result in a higher penalty for differences in brightness
            distribution. For our experiments we used p = 1.
        """
        self.p = p
        self.verbose = verbose

    @staticmethod
    def min_valuable() -> float:
        return 0.6

    def apply(self, img_path) -> float:
        im = cv2.imread(img_path)
        # calculate mean value from RGB channels and flatten to 1D array
        # HSV -> axis 2 is value = brightness
        vals = im.mean(axis=2).flatten()
        # plot histogram with 255 bins
        b, bins, patches = plt.hist(vals, 9)

        img = cv2.imread(img_path, 0)
        rows, cols = img.shape
        N = rows * cols

        b = sorted(b, reverse=True)
        result = M_benfort(b, self.p, N)
        if self.verbose:
            print(f"[Measure] Benfort measure (luminance distribution): {result}")

        return result