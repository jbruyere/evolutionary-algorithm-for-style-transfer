import numpy as np
from PIL import Image

class Color:
    R = 0
    G = 1
    B = 2


class AestheticMeasure:
    """
        Compute a measure of Aesthetics
        And provide usefull color methods
    """

    @staticmethod
    def red(pixel):
        return int(pixel[Color.R])

    @staticmethod
    def green(pixel):
        return int(pixel[Color.G])

    @staticmethod
    def blue(pixel):
        return int(pixel[Color.B])

    @staticmethod
    def intensity(pixel):
        return round((AestheticMeasure.red(pixel) + AestheticMeasure.green(pixel) + AestheticMeasure.blue(pixel)) / 3)

    @staticmethod
    def luminance(pixel):
        return round(0.3 * pixel[Color.R] + 0.59 * pixel[Color.G] + 0.11 * pixel[Color.B])

    @staticmethod
    def open_PIL_image(img_path):
        img = Image.open(img_path)
        pixels = np.array(img)
        img.close()
        return pixels

    def apply(self, image_path) -> float:
        """
            :return: a float number between 0 and 1, the bigger the better
        """
        pass

    @staticmethod
    def min_valuable() -> float:
        """
            :return: a float value between 0 and 1, the value that is the minimum to be considered as valuable
        """
        pass
