import os
import argparse
import sys

sys.path.append(".")
from src.moea import NeuralEvolutionaryArtStyleTransfer


def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--continue_execution", default=False, action='store_true',
                        help="Continue a started algorithm stored in --save path.")
    parser.add_argument("-e", "--epochs", default=500, type=int,
                        help="Number of epochs applied during Style Transfer.")
    parser.add_argument("-f", "--save_file", default="main_save", type=str,
                        help="Name of the main file to be saved.")
    parser.add_argument("-g", "--generations", default=10, type=int,
                        help="Number of generations used in the Evolutionary Algorithm.")
    parser.add_argument("-i", "--input", default="dataset/input_images",
                        help="Path to the directory containing the input artworks.")
    parser.add_argument("-l", "--load_file", default="main_save", type=str,
                        help="Name of the main file to be loaded if --continue_execution is passed.")
    parser.add_argument("-m", "--max_individuals", default=50, type=int,
                        help="Maximum number of individuals per generations."
                             " Should be an even number. Should be bigger than 2X (selection_x).")
    parser.add_argument("-o", "--output", default="dataset/output_images",
                        help="Path to the output directory.")
    parser.add_argument("-p", "--processes", default=1, type=int,
                        help="Number of processes used during cross-over. (Run several NST in parallel.)"
                             " Should be lower than the number of CPUs/GPUs on the machine.")
    parser.add_argument("-r", "--results", default="dataset/results",
                        help="Path to the result directory.")
    parser.add_argument("-s", "--save", default="save", type=str,
                        help="Path to the save directory.")
    parser.add_argument("-t", "--temporary", default="dataset/tmp_images",
                        help="Path to the temporary directory.")
    parser.add_argument("-u", "--unique", default=False, action='store_true',
                        help="The selected individuals are unique (selected only once) during the first generation.")
    parser.add_argument("-v", "--verbose", default=0, type=int,
                        help="Level of verbosity. Display information about what happened if verbose > 0."
                             " Display mutation and measures if verbose > 1.")
    parser.add_argument("-x", "--selection_x", default=5, type=int,
                        help="In tournament selection, the 2 best individuals over X random individuals are selected."
                             " Should be bigger with a bigger number of individuals, and vice-versa."
                             " In rank selection, the bigger X is, the lower the difference of "
                             "selection probability is between the individuals.")
    return parser.parse_args()


def main():
    args = parse_arguments()
    inputs = [os.path.join(args.input, f) for f in os.listdir(args.input) if os.path.isfile(os.path.join(args.input, f))]
    neast = NeuralEvolutionaryArtStyleTransfer(
        save_dir=args.save,
        save_file=args.save_file,
        load_file=args.load_file,
        continue_moea=args.continue_execution,
        verbose=args.verbose
    )
    neast.run_moea(
        args.output,
        args.temporary,
        args.results,
        args.generations,
        args.epochs,
        args.max_individuals,
        args.selection_x,
        verbose=args.verbose,
        inputs=inputs,
        unique=args.unique,
        nb_processes=args.processes
    )


if __name__ == "__main__":
    main()
